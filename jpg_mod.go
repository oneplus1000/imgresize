package imgresize

import (
	//"fmt"
	"image"
	"image/jpeg"
	"io"
	"path/filepath"
	"strings"
)

type JpgMod struct {
}

func (me *JpgMod) IsThisType(filename string) bool {
	ext := filepath.Ext(filename)
	ext = strings.ToLower(strings.TrimSpace(ext))
	//fmt.Printf("%s\n\n\n", ext)
	if ext == ".jpg" || ext == ".jpeg" {
		return true
	}
	return false
}

func (me *JpgMod) CreateImg(src io.Reader) (image.Image, error) {
	img, err := jpeg.Decode(src)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func (me *JpgMod) EncodeImg(imgthumb image.Image, dest io.Writer) error {
	return jpeg.Encode(dest, imgthumb, nil)
}
