package imgresize

import (
	"errors"
	"github.com/nfnt/resize"
	"image"
	"io"
)

var ERROR_NO_IMAGE_TYPE_FOUND = errors.New("no image type found")

type Resizer struct {
	mods []IMod
}

var _resizer *Resizer

func GetResizer() *Resizer {
	if _resizer == nil {
		_resizer = new(Resizer)
		_resizer.RegitMod(new(JpgMod))
		_resizer.RegitMod(new(PngMod))
	}
	return _resizer
}

func (me *Resizer) RegitMod(mod IMod) {
	me.mods = append(me.mods, mod)
}

func (me *Resizer) MakeThumb(info *ImgInfo, src io.Reader, dest io.Writer) error {
	var mod IMod
	for _, m := range me.mods {
		if m.IsThisType(info.FileName) {
			mod = m
			break
		}
	}

	if mod == nil {
		return ERROR_NO_IMAGE_TYPE_FOUND
	}

	img, err := mod.CreateImg(src)
	if err != nil {
		return err
	}

	thumb := resize.Thumbnail(info.Width, info.Height, img, resize.Lanczos3)
	err = mod.EncodeImg(thumb, dest)
	if err != nil {
		return err
	}
	return nil
}

//IMod คือว่า mod ย่อมาจาก module
type IMod interface {
	IsThisType(filename string) bool
	CreateImg(src io.Reader) (image.Image, error)
	EncodeImg(imgthumb image.Image, dest io.Writer) error
}

//imginfo
type ImgInfo struct {
	FileName string
	Width    uint
	Height   uint
}
