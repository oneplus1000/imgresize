package imgresize

import (
	//"fmt"
	"image"
	"image/png"
	"io"
	"path/filepath"
	"strings"
)

type PngMod struct {
}

func (me *PngMod) IsThisType(filename string) bool {
	ext := filepath.Ext(filename)
	ext = strings.ToLower(strings.TrimSpace(ext))
	if ext == ".png" {
		return true
	}
	return false
}

func (me *PngMod) CreateImg(src io.Reader) (image.Image, error) {
	img, err := png.Decode(src)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func (me *PngMod) EncodeImg(imgthumb image.Image, dest io.Writer) error {
	return png.Encode(dest, imgthumb)
}
